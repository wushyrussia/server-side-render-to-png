A utility designed to generate png from an html page.

to do this:
- static resources are imported
- the http server is being started
- when using puppeteer, a page received from the server opens
- all js scripts on the page are executed
- the final page turns into a png file