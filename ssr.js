//A utility designed to generate png from an html page.
// to do this:
// - static resources are imported
// - the http server is being started
// - when using puppeteer, a page received from the server opens
// - all js scripts on the page are executed
// - the final page turns into a png file

const puppeteer = require('puppeteer');
const fs = require('fs');
const express = require('express')
const app = express()

// ******************** this part necessary if you want to render a local file with js ********************
let file = fs.readFileSync('/folder/some_static_file', 'utf8');
let html = fs.readFileSync('index.html', 'utf8');

app.use(express.static('data'));
app.get('/folder/some_static_file', (req, res) => {
    res.send(file)
})

app.get('/', (req, res) => {
    res.send(html)
})

app.listen(8081);
// ********************************************************************************************************


ssr("http:localhost:8081")

async function ssr(url) {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()
    await page.setViewport({ width: 1280, height: 800 })
    await page.goto(url)
    await page.screenshot({ path: 'mine.png', fullPage: true })
    await browser.close()
    process.exit(0)
}

